#ifndef __HW_FLAGS_H
#define __HW_FLAGS_H

/* dma */
#define DMA_LOOPBACK_ENABLE 0x1

#define DMA_TABLE_LOOP_INDEX 1 << 0
#define DMA_TABLE_LOOP_COUNT 1 << 16

#define DMA_CHANNEL_TX 1
#define DMA_CHANNEL_RX 2

#endif /* __HW_FLAGS_H */
